package com.mycompany.weatherapp;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This test class ensures that the getCmd and getProcess methods of the
 * ProcessBuilder class work as expected with non-existing and existing files.
 * In the case of invalid files, it validates the type of the exception thrown
 * (IOException), as well as the content of the error message.
 *
 * @author David Pizzolongo
 */
public class ProcessBuilderDht11Test {

    private ProcessBuilderDht11 processBuilder;

    public ProcessBuilderDht11Test() {
    }

    @Test
    public void testGetProcessValid() {
        String cmd = "src/main/Cpp/DHT11";
        processBuilder = new ProcessBuilderDht11(cmd);

        try {
            assertEquals(cmd, processBuilder.getCmd());
            assertNotNull(processBuilder.getProcess());
        } catch (IOException ex) {
            System.out.println("Test failed. A valid executable file was provided.");
        }
    }

    // test startThread with other executables
    /* Path is valid (file exists), but output is invalid (does not display the humidity and temperature of the room).
       Despite this, no exception is thrown, but in the WeatherController, the lines of output from this file will be ignored. */
    @Test
    public void testGetProcessValid2() {
        String joystickCmd = "src/main/resources/test_files/Joystick";
        processBuilder = new ProcessBuilderDht11(joystickCmd);

        try {
            assertEquals(joystickCmd, processBuilder.getCmd());
            assertNotNull(processBuilder.getProcess());
        } catch (IOException e) {
            System.out.println("Test failed. IOException should not be thrown.");
        }
    }

    @Test
    public void testGetProcessValid3() {
        String ledCmd = "src/main/resources/test_files/ButtonLED";
        processBuilder = new ProcessBuilderDht11(ledCmd);

        try {
            assertEquals(ledCmd, processBuilder.getCmd());
            assertNotNull(processBuilder.getProcess());
        } catch (IOException e) {
            System.out.println("Test failed. IOException should not be thrown, since the executable file provides output.");
        }
    }

    @Test
    public void testGetProcessInvalidCmd() {
        try {
            String emptyCmd = "";
            processBuilder = new ProcessBuilderDht11(emptyCmd);

            assertEquals(emptyCmd, processBuilder.getCmd());
            processBuilder.getProcess();
        } catch (IOException e) {
            System.out.println("Test passed! Process could not be started as the file is not an executable.");
        }
    }

    @Test
    public void testGetProcessInvalidFile() {
        try {
            String invalidCmd = "src/main/Executable";
            processBuilder = new ProcessBuilderDht11(invalidCmd);

            assertEquals(invalidCmd, processBuilder.getCmd());
            processBuilder.getProcess();
        } catch (IOException ex) {
            System.out.println("Test passed! File does not exist.");
        }
    }

    /* test startThread method with a cmd that gives no output. Since it is an invalid file or directory,
       an IOException is thrown and an appropriate error message is logged to the user. */
    @Test
    public void testGetProcessCmdNoOutput() {
        String theCmd = "ls -l";
        processBuilder = new ProcessBuilderDht11(theCmd);
        try {
            processBuilder.getProcess();
        } catch (IOException e) {
            System.out.println("Test passed!");

            String expectedError = "No such file or directory";
            assertTrue(e.getMessage().contains(expectedError));
        }
    }

    @Test
    public void testGetProcessInvalidPath() {
        String invalidPath = "srccc/main/Cpp/DHT11";
        processBuilder = new ProcessBuilderDht11(invalidPath);

        assertThrows(IOException.class, () -> {
            processBuilder.getProcess();
        });

    }

    @Test
    public void testGetProcessInvalidPath2() {
        String invalidInput = "src\\main\\Cpp\\DHT11";          // back slash used in path instead of forward slash
        processBuilder = new ProcessBuilderDht11(invalidInput);

        assertThrows(IOException.class, () -> {
            processBuilder.getProcess();
        });
    }

}
