
package com.mycompany.weatherapp.dataObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test ObjecMapeper
 * @author "Grigor Mihaylov"
 */
public class WeatherMapperTest {
    
   
    private WeatherMapper mapperTest;
    private List<City> testList;
    private WeatherJson weather;
    
    @BeforeEach
    public void setUp() {
        mapperTest = new WeatherMapper();
    }
    
    @Test
    public void getCitiesTest(){
        testList=mapperTest.getCities();
        assertNotNull(testList);
    }
    
    @Test
    public void getWeatherJsonTest(){
        Map<String, Double> coordinates = new HashMap<>();
        coordinates.put("lat", 15.0);
        coordinates.put("lon", 45.0);
        weather = mapperTest.getWeatherJson(coordinates);
        assertNotNull(weather);
    }
    
}
