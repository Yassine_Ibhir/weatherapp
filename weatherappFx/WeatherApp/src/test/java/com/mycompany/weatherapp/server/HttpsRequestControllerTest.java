package com.mycompany.weatherapp.server;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests the sendRequest method from the HttpsRequestController class,
 * with valid and invalid input. The API key, required for the connection, is set as final. 
 * Before each test, an instance of the request class is initialized. 
 * @author David Pizzolongo
 */
public class HttpsRequestControllerTest {
    
    private HttpsRequestController request;
    // enter API key here
    private final String appKey = "f7de5b9c4a694ea2687d1d0d1ddbd9de";              

    public HttpsRequestControllerTest() {
    }

    @BeforeEach
    public void setUp() {
        request = new HttpsRequestController();
    }

    /* 
       Tests sendRequest with a valid URL (valid FQDN and a valid query string). 
       The query string has latitude and longitude parameters, and specifies that 
       the response contains the current and daily forecast for the next seven days. 
    */
    @Test
    public void testSendRequestValidUrl() {
        String website = "http://api.openweathermap.org/data/2.5/onecall";
        String queryStr = "?lat=33.44&lon=-94.04&exclude=minutely,hourly&appid=" + appKey;

        String response = request.sendRequest(website, queryStr);

        assertFalse(response.equals(""));       // ensures that response is not empty
        assertTrue(response.contains("daily")); 
        assertFalse(response.contains("hourly"));  
    }
    
    /* 
       Tests sendRequest method with a valid URL. The url contains latitude and longitude information 
       and the appId required for the request. This test ensures that the response is not an 
       empty string and the latitude is present in the response.
    */
    @Test
    public void testSendRequestValidUrl2() {
        

        String website = "http://api.openweathermap.org/data/2.5/weather";
        String query = "?lat=15&lon=45&appid=" + appKey;
        String response = request.sendRequest(website, query); 

        assertFalse(response.equals(""));
        assertTrue(response.contains("15"));
    }

    /* 
       Tests sendRequest with a valid website and query string. The query ensures that 
       the response contains the weather for Memphis, a city in the US, and the forecast
       temperatures are in Celsius. The test verifies that the response contains Memphis.
    */
    @Test
    public void testSendRequestValidUrl3(){
        String website = "http://api.openweathermap.org/data/2.5/weather";
        String query = "?q=Memphis,us&units=metric&appid=" + appKey;
        String response = request.sendRequest(website, query); 

        assertTrue(response.length() > 0);
        assertTrue(response.contains("Memphis"));
    }

    /* 
       Tests sendRequest with an invalid website and query string, both of which are empty.
       It makes sure that the response is empty as well.
    */
    @Test
    public void testSendRequestInvalidUrl() {
        String response = request.sendRequest("", "");
      
        assertEquals("", response);
    }

    /* 
       Tests sendRequest with an invalid website and an invalid query string, which
       contains more than one & and duplicate = after q key. Since both of them combined create an invalid url, 
       the response is empty and the error message for invalid parameters is shown.
    */
    @Test
    public void testSendRequestInvalidUrl2() {
        String response = request.sendRequest("www.weather", "?q==Alaska&&appid=" + appKey);
        
        assertEquals("", response);
    }
    
    /* 
       Tests sendRequest with an invalid/incomplete website and a valid query string.
       Since one of them is invalid, the response is empty and 
       the error message for an IOException is displayed.
    */
    @Test
    public void testSendRequestInvalidWebsite() {
        String response = request.sendRequest("http://api", "?q=Montreal,ca&appid=" + appKey);
        
        assertEquals("", response);
    }
    
    /* 
       Tests sendRequest with an valid website and an invalid query string (number in key). 
       The test ensures that the returned response is empty 
       and an IllegalArgumentException is thrown, since the regex pattern is not matched.
    */
    @Test
    public void testSendRequestInvalidParams() {
        String website = "http://api.openweathermap.org/data/2.5/weather";
        String invalidQuery = "?la9t=50&lon=-75&appid=" + appKey;
        String response = request.sendRequest(website, invalidQuery);
        
        assertEquals("", response);
    }
    
    /* 
       Tests sendRequest with an valid website and a query string with duplicate parameters.
       The query string contains a valid pattern, but since the appid parameters are redundant,
       an invalid response code message is displayed and the response will be an empty string.
    */
    @Test
    public void testSendRequestIncompatibleParams(){
        String website = "http://api.openweathermap.org/data/2.5/weather";
        String invalidQuery = "?q=Melbourne,au&appid=gh6r2qkaoeoqpa&appid=" + appKey;
        String response = request.sendRequest(website, invalidQuery); 

        assertEquals("", response);
    }
}
