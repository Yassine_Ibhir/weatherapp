
package com.mycompany.weatherapp.dataObject;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *  Test City methods
 * @author Yassine Ibhir
 */
public class CityTest {
    
    private City city; 
    
    @BeforeEach
    public void setUp() {
        city = new City();
    }

    /**
     * Test of getId method, of class City.
     */
    @Test
    public void testGetId() {
        int expResult = 0;
        int result = city.getId();
        assertEquals(expResult, result);
    }

   

    /**
     * Test of getName method, of class City.
     */
    @Test
    public void testGetName() {
       
        city.setName(("Montreal"));
        String result = city.getName();
        String expResult = "Montreal";
        assertEquals(expResult, result);
    }

   

    /**
     * Test of getCountry method, of class City.
     */
    @Test
    public void testGetCountry() {
        city.setCountry(("CA"));
        String result = city.getCountry();
        String expResult = "CA";
        assertEquals(expResult, result);
    }

  

    /**
     * Test of getCoord method, of class City.
     */
    @Test
    public void testGetCoord() {
      
        Map<String, Double> coordinates = new HashMap<>();
        coordinates.put("lat", 2.6);
        coordinates.put("lon", 5.6);
        city.setCoord(coordinates);
        Map<String, Double> result = city.getCoord();
        assertEquals(coordinates, result);
    }

   

    /**
     * Test of getState method, of class City.
     */
    @Test
    public void testGetState() {
      
        String expResult = "NY";
        city.setState("NY");
        String result = city.getState();
        assertEquals(expResult, result);
    }

  
    /**
     * Test of toString method, of class City
     * to set if it removes the accent.
     */
    @Test
    public void testToString() {
        //String cityStr = name+" "+country+" "+state+" #"+id;
        
        String expResult = "Montreal CA null #12345";
        city.setName("Montréal");
        city.setCountry("CA");
        city.setId(12345);
        String result = city.toString();
        assertEquals(expResult, result);
      
    }
    
}
