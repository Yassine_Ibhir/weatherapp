
package com.mycompany.weatherapp.dataObject;

/**
 *  an data class to help with the conversion from json
 * @author "Grigor Mihaylov & Yassine Ibhir"
 */
public class Daily {
    
    private Temperature temp;
    private Double humidity;
    private long dt;
    private long sunrise;
    private long sunset;
    private Double wind_speed;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(int sunrise) {
        this.sunrise = sunrise;
    }

    public long getSunset() {
        return sunset;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

    public Double getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(Double wind_speed) {
        this.wind_speed = wind_speed;
    }

    @Override
    public String toString() {
        return "Daily{" + "temp=" + temp + ", humidity=" + humidity + ", weather=" + weather[0] + '}';
    }
    private Weather[] weather;

    public Temperature getTemp() {
        return temp;
    }

    public void setTemp(Temperature temp) {
        this.temp = temp;
    }

 
    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }
    
}
