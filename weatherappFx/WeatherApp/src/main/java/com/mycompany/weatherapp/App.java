package com.mycompany.weatherapp;

import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;

import javafx.stage.Stage;

/**
 * Main class
 * JavaFX Weather App
 * @author David,Grigor, and Yassine
 */
public class App extends Application {

   public static Stage theStage;

    @Override
    public void start(Stage stage) throws IOException{
        
        MainWeatherScreen screen = new MainWeatherScreen();
        
        var scene = new Scene(screen, 1406, 1040);
        
        // initialize controller with the screen
        WeatherController controller = new WeatherController(screen);
        // start 
        controller.interact();
        
        App.theStage = stage;
        
        //Set the active scene
        theStage.setScene(scene);
        // set the title of the window
        theStage.setTitle("Weather App");
        theStage.show();
        
        // Make sure the application quits completely on close
        theStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

    }
    
    /**
     * main method
     * @param args 
     */
    public static void main(String[] args) {
        launch();
    }

}
