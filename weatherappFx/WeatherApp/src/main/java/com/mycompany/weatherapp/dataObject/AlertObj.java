
package com.mycompany.weatherapp.dataObject;

/**
 * class defines the alert object
 * @author Yassine Ibhir
 */
public class AlertObj {
    private String description;
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
