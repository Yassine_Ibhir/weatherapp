
package com.mycompany.weatherapp.dataObject;

/**
 * an data class to help with the conversion from json
 * @author "Grigor Mihaylov"
 */
public class WeatherJson {
    private Current current;
    private Daily[] daily;
    private AlertObj[] alerts;
    private String timezone;
    
    
    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
    

    public AlertObj[] getAlerts() {
        return alerts;
    }

    public void setAlerts(AlertObj[] alerts) {
        this.alerts = alerts;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    public Daily[] getDaily() {
        return daily;
    }

    public void setDaily(Daily[] daily) {
        this.daily = daily;
    }
    
}
