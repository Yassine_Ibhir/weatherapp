
package com.mycompany.weatherapp.dataObject;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * defines City object used for searching
 * cities
 * @author Yassine Ibhir
 */
public class City {
    
    private int id;
    private String name;
    private String state;
    private String country;
    private Map<String, Double> coord;
  
    // getters & setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Map<String, Double> getCoord() {
        return coord;
    }

    public void setCoord(Map<String, Double> coord) {
        this.coord = coord;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    /**
     * returns a string representation of a city
     * without accents
     * @return string
     */
    @Override
    public String toString() {
        
        String cityStr = name+" "+country+" "+state+" #"+id;
        // removes accents from the string (Montréal becomes Montreal)
        String m = StringUtils.stripAccents(cityStr);
        
        return m;
    }
      
}
