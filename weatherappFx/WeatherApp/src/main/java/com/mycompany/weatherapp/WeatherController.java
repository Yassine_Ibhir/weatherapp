
package com.mycompany.weatherapp;


import com.mycompany.weatherapp.dataObject.WeatherMapper;
import javafx.scene.control.*;
import org.controlsfx.control.textfield.TextFields;
import javafx.scene.layout.VBox;
import com.mycompany.weatherapp.dataObject.*;
import eu.hansolo.tilesfx.Tile;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;


/**
 * This class controls the events
 * and responds to user interaction
 * has access to the model and view
 * @author Yassine Ibhir
 */
public class WeatherController {
    
    // instance of the main screen
    private MainWeatherScreen weatherScreen;
    // choice box for current and 7 days weather
    private ChoiceBox choiceBox;
    // city Field
    private TextField cityField;
    // text area to display output of dht11
    private TextArea dht11TextArea;
    // vboxes for 7 days forecast
    private VBox[] vBdaily = new VBox[7];
    // current weather vbox
    private VBox currentWeatherVbox;
    // vbox alert
    private VBox alert;
    // exit button
    private Button exit;
    // update button
    private Button update;
    //submit button
    private Button submit;
    //clear text button
    private Button clear;
    //dailyTiles to change title to date
    private Tile[] dailyTiles;
    //current weather tile to change title to current date and time
    private Tile currentWeatherTile;
    
    private static boolean isFinish = false;
    
    // Process
    private Process process;
    //This hashMap is used to get the city object by city id
    private Map<Integer,City> mapedCities = new HashMap<>();
    
    // weather mapper for jackson Api
    private WeatherMapper weatherMapper = new WeatherMapper();
    
    // one call weather json data
    private WeatherJson weatherJson;
    
    // path to images
    private static String imagesPath = "src//main//resources//images//";
    
    // setup a Logger used to log IllegalState and IO exceptions
    private static final Logger LOGGER = Logger.getLogger(WeatherController.class.getName());
    
    /**
     * Initialize all components for interacting with screen
     * @param mws MainWeatherscreen class
     */
    public WeatherController(MainWeatherScreen mws){
        
        // initialize all private fields
        this.weatherScreen = mws;
        this.choiceBox = mws.getChoiceBox();
        this.cityField = mws.getCityField();
        this.dht11TextArea = mws.getDht11TextArea();
        this.vBdaily = mws.getDaily();
        this.currentWeatherVbox = mws.getCurrentWeather();
        this.alert = mws.getAlert();
        this.dailyTiles = mws.getDailyTiles();
        this.currentWeatherTile = mws.getCurrentWeatherTile();
        
        this.exit = mws.getExit();
        this.update = mws.getUpdate();
        this.submit= mws.getSubmit();
        this.clear= mws.getClear();
    }
    
   /**
    * start interacting with the user
   * @author Yassine Ibhir
   */
    public void interact() {
        startProcess();
        startThread();
        setAutoCompletion();
        setEvents();
    }
    /**
     * get the process from the process Builder
     * @author David & grigor
     */
    private void startProcess() {
        String theCmd = "src/main/Cpp/DHT11_faster";
        try{
            ProcessBuilderDht11 processBuilder = new ProcessBuilderDht11(theCmd);
            this.process = processBuilder.getProcess();  
        }
       
        catch(IOException e){
            System.out.println("Process failed: " + e.getMessage()); 
        }
    }
    /**
     * start the thread that displays the DHT11 data
     * @author Grigor, david
     */
    private void startThread(){

        Thread thread = new Thread() {
            @Override
            public void run() {
                //input buffer for the cli app
                BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));

                String line = null;
                
                try {
                    while ((line = input.readLine()) != null && isFinish != true) {
                        //if a line is found prosses it
                        if(validateInput(line) != null){
                            updateInput(line);
                        }
                    }
                    // close the BufferedReader
                    if (isFinish == true) {
                        input.close();
                    }

                } catch (IOException ex) {
                    Logger.getLogger(WeatherController.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Thread interept due to IOException.");

                    // an error message is displayed to the user in the tile
                    dht11TextArea.setText("Humidity and temperature could not be displayed.");
                }

            }
        };

        thread.start();
    }
    /**
     * update the tile with the DHT11 data
     * using Platforms
     * @author Grigor , yassine, david
     */
    private void  updateInput(String line){
        String[] values = line.split(",");
        if (values.length != 2) {
            LOGGER.log(Level.SEVERE, "Unexpected results in updateInput()");
            throw new IllegalStateException("Invalid output format, data incomplete.");
        }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //get the time stamp
                var timeStamp = new Date();

                int startIndex = values[0].indexOf("is") + 2;
                String humidity = values[0].substring(startIndex);  // retrieves the humidity in %

                startIndex = values[1].indexOf("is") + 2;
                String temp = values[1].substring(startIndex);      // retrieves the temperature in ℃

                dht11TextArea.setText("\n\nAt: " + timeStamp + "\nHumidity: " + humidity + "\tTemperature: " + temp);
            }
        });
    }
    
    /**
     * This method normalizes and validates each line of output from the process
     * input stream, before passing the data to updateScreen(). If the line is
     * valid, the number of invalid lines is reset to its default value. If not,
     * the invalidCount is incremented by 1.
     *
     * @param {line} each line of output from the socket
     * @param {validPattern} compiled pattern used for string validation
     * @return {theLine} returns the completely validated line or if invalid, returns null
     */
    private String validateInput(String line) {
        String theLine = Normalizer.normalize(line, Normalizer.Form.NFKC);

        /* the string from the input stream must begin with an uppercase letter, followed by one or more 
           characters, and end with a *C (degrees Celsius). */
        Pattern validPattern = Pattern.compile("^[A-Z].+\\*C$");
        
        Matcher matcher = validPattern.matcher(theLine);

        // the line of input must also start with the word Humidity in order to be valid
        if (theLine.startsWith("Humidity") && matcher.find()) {
            return theLine ;

            
        }
        return null;
    }
    
    

    /**
     * Uses WeatherMapper to get all cities
     * binds for autoCompletion and stores
     * cities in hashMap
     * @author Yassine Ibhir
     */
    private void setAutoCompletion() {

        List<City> cities = weatherMapper.getCities();
        
        // put cities in hashmap using key(id) value(City)
        for (City city : cities){
            
            mapedCities.put(city.getId(), city);
        }
        //sets auto completion    
        TextFields.bindAutoCompletion(cityField, cities);
    }
    
    /**
     * set events and callbacks
     * @author Yassine Ibhir
     */
    private void setEvents(){
        submit.setOnAction(a -> requestWeather());
        update.setOnAction(a -> requestWeather());
        exit.setOnAction(a -> endApplication());
        clear.setOnAction(a -> clearTextField());
    }
 
    
    /**
     * request Weather Data
     * @Yassine Ibhir
     */
    private void requestWeather(){
        
        int validIdKey = getCityId();
        
        if(validIdKey != 0 ){
            Map<String,Double> coord = mapedCities.get(validIdKey).getCoord();
            weatherJson = weatherMapper.getWeatherJson(coord);
            displayWeather();
        }
       
    }
    
    /**
     * defines which data to display based on what
     * user chooses
     * @author Yassine
     */
    private void displayWeather(){
        
        String choice = this.choiceBox.getValue().toString();
        if (choice.equals("Current Weather")){
            // clear texts all texts of 7 days forecasts
            clearDailyForecastData();
            // clear title text of 7 days Tiles
            clearDailyTileTitles();
            // display CurrentWeather
            displayCurrentWeather();
        }
        else{
            // clear everything in currentWeather tile
            clearCurrentWeatherData();
            clearCurrentWeatherTile();
            // display daily Forecasts
            displaydailyForecast();
        }
        //display alert 
        diplayAlert();
            
    }
    
   
    
    /**
     * method to display alert description
     * and time zone
     * @author Yassine
    */
    
    private void diplayAlert(){
        
        //get time zone of the requested city and display it
        String timeZone =  weatherJson.getTimezone();
        Label timeZ = (Label) this.alert.getChildren().get(0);
        timeZ.setText("Time Zone: "+timeZone);
        
        // get alerts of city
        AlertObj[] alerts = weatherJson.getAlerts();
        
        // get textArea 
        TextArea desc = (TextArea) this.alert.getChildren().get(1);
        
        // display alerts
        if (alerts != null){
            for(AlertObj a : alerts){
                desc.appendText(a.getDescription());
            }  
        }
        else{
            desc.setText("Description : No Weather alert :) ");
        }  
    }
    
    /**
     * display current weather data
     * @author Yassine
     */
    private void displayCurrentWeather(){
        
        // Set current date and time to tile title
        long dt = weatherJson.getCurrent().getDt();
        String timeZ = weatherJson.getTimezone();
        String theDate = convertUnixTime(dt, timeZ,"E, dd MMM yyyy hh:mm aa");
        currentWeatherTile.setTitle(theDate);   
       
        // getData of image and diplay it
        ImageView imgView = (ImageView) currentWeatherVbox.getChildren().get(0);
        String icon = weatherJson.getCurrent().getWeather()[0].getIcon();
        displayImage(imgView, icon );

        // get data of weather description and display it
        String description = weatherJson.getCurrent().getWeather()[0].getDescription();
        Label desc = (Label) currentWeatherVbox.getChildren().get(1);
        desc.setText(description);

        // getDatA of temperature and dispaly It
        String temperature = weatherJson.getCurrent().getTemp().toString();
        Label temp = (Label) currentWeatherVbox.getChildren().get(2);
        temp.setText(temperature+" C");

        // getData of humidity and dispaly It
        String humidity = weatherJson.getCurrent().getHumidity().toString();
        Label hum = (Label) currentWeatherVbox.getChildren().get(3);
        hum.setText("Humidity: "+humidity+" %");

         // getData of humidity and dispaly It
        String feelsLike = weatherJson.getCurrent().getFeels_like().toString();
        Label feels = (Label) currentWeatherVbox.getChildren().get(4);
        feels.setText("Feels Like: "+feelsLike+" C");
        
        // getData of windSpeed and display it
        String wSpeed = weatherJson.getCurrent().getWind_speed().toString();
        Label windSpeed = (Label) currentWeatherVbox.getChildren().get(5);
        windSpeed.setText("Wind Speed: "+wSpeed+" metre/sec");
        
        // get hbox for sunset and sunrise
        HBox sun = (HBox) currentWeatherVbox.getChildren().get(6);
        //timeZone
        String timeZone = weatherJson.getTimezone();
        
        Label sunRise = (Label) sun.getChildren().get(0);
        long sunR = weatherJson.getCurrent().getSunrise();
        String sunRiseTime = convertUnixTime(sunR, timeZone, "hh:mm aa");
        sunRise.setText("Sunrise "+sunRiseTime);
        
        Label sunSet = (Label) sun.getChildren().get(1);
        long sunS = weatherJson.getCurrent().getSunset();
        String sunSetTime = convertUnixTime(sunS, timeZone, "hh:mm aa"); 
        sunSet.setText("Sunset "+sunSetTime);

    }
    
    /**
     * display daily weather Forecast
     * @author yassine
     */
    private void displaydailyForecast(){
        Daily[] dailyForecast = weatherJson.getDaily();

        for(int i=0; i<vBdaily.length; i++){
            
            // Set Tile title as a date
            Tile tileDay = dailyTiles[i];
            long dt = dailyForecast[i+1].getDt();
            String timeZ = weatherJson.getTimezone();
            String theDate = convertUnixTime(dt, timeZ,"E, dd MMM yyyy");
            tileDay.setTitle(theDate);
            
            // get Vbox
            VBox vb = vBdaily[i];
            //set image
            ImageView imgView = (ImageView) vb.getChildren().get(0);
            String icon = dailyForecast[i+1].getWeather()[0].getIcon();
            displayImage(imgView, icon );

            // get data of weather description and display it
            String description = dailyForecast[i+1].getWeather()[0].getDescription();
            Label desc = (Label) vb.getChildren().get(1);
            desc.setText(description);

            // get data max temperature and display it
            String maxTemp = dailyForecast[i+1].getTemp().getMax().toString();
            Label tempMax = (Label) vb.getChildren().get(2);
            tempMax.setText("Max: "+maxTemp+ " C");

            // get data mix temperature and display it
            String minTemp = dailyForecast[i+1].getTemp().getMin().toString();
            Label tempMin = (Label) vb.getChildren().get(3);
            tempMin.setText("Min: "+minTemp+ " C");

            // get data humidity and display it
            String humi = dailyForecast[i+1].getHumidity().toString();
            Label humidity = (Label) vb.getChildren().get(4);
            humidity.setText("Humidity: "+humi+ " %");
            
             // getData of windSpeed and display it
            String wSpeed = dailyForecast[i+1].getWind_speed().toString();
            Label windSpeed = (Label) vb.getChildren().get(5);
            windSpeed.setText("Wind Speed: "+wSpeed+" metre/sec");
            
            HBox sun = (HBox) vb.getChildren().get(6);
            
            //timeZone
            String timeZone = weatherJson.getTimezone();
            
            // display sunrise
            Label sunRise = (Label) sun.getChildren().get(0);
            long sunR = dailyForecast[i+1].getSunrise();
            String sunRiseTime = convertUnixTime(sunR, timeZone, "hh:mm aa");
            sunRise.setText("Sunrise "+sunRiseTime);
            
            //display sunset
            Label sunSet = (Label) sun.getChildren().get(1);
            long sunS = dailyForecast[i+1].getSunset();
            String sunSetTime = convertUnixTime(sunS, timeZone, "hh:mm aa"); 
            sunSet.setText("Sunset "+sunSetTime);
        }
        
    }
     

    /**
     * displays the icon image
     * @param imgView image View of any day
     * @param img string of the image name
     * @author Yassine
     */
    private void displayImage(ImageView imgView, String img){
        
        String m = img+".png";
        try {
            InputStream stream = new FileInputStream(imagesPath+m);
            Image icon = new Image(stream);
            imgView.setImage(icon);
        } catch (FileNotFoundException ex) {
            System.out.println("Invalid path to image.");
        }
    }
    
    /**
     * clear textField
     * @Yassine Ibhir
     */
    private void clearTextField(){
        this.cityField.setText("");
    }
    
    /**
     * returns city id
     * @return city id or 0 
     * @Yassine Ibhir
     */
    private int getCityId(){
        int id = getId();
        if(id <= 0 || !mapedCities.containsKey(id)){
            alertWindow("Please type and choose a valid City Name");
            clearTextField();
            return 0;
        }
        
        return id;
    }
    
    /**
     * validate and returns the id
     * @return
     * @Yassine Ibhir
     */
    private int getId(){
        
        // text field value includes city Id
        String city = cityField.getText();
        // extract id from the string 
        String id = city.substring(city.lastIndexOf("#")+1);
        
        //parse to int
        try{
            int cityId = Integer.parseInt(id);
            return cityId; 
        }
        // not a valid number
        catch (NumberFormatException e){
            return 0;
        }

        
    }
    
     /**
     * pops up a window with error message 
     * @param message 
     * @Yassine Ibhir
     */
    private void alertWindow(String message){
        
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setTitle("Read the message below");
        a.setContentText(message);
        a.setResizable(true);
        a.showAndWait();
    }
    
    /**
     * clear all 7 days weather forecasts
     * texts and images 
     * data
     * @Yassine Ibhir
     */
    private void clearDailyForecastData(){
        
        for (VBox vb : vBdaily) {
            
            ImageView imgView = (ImageView) vb.getChildren().get(0);
            imgView.setImage(null);
            
            
            Label desc = (Label) vb.getChildren().get(1);
            desc.setText("");
            
            
            Label tempMax = (Label) vb.getChildren().get(2);
            tempMax.setText("");

            Label tempMin = (Label) vb.getChildren().get(3);
            tempMin.setText("");
            
            
            Label humidity = (Label) vb.getChildren().get(4);
            humidity.setText("");
            
            Label windSpeed = (Label) vb.getChildren().get(5);
            windSpeed.setText("");
            
            HBox sun = (HBox) vb.getChildren().get(6);
            
            Label sunSet = (Label) sun.getChildren().get(0);
            sunSet.setText("");
            Label sunRise = (Label) sun.getChildren().get(1);
            sunRise.setText("");
        }
    }
    
    /**
     * clears current weather data
     * @author yassine
     */
    private void clearCurrentWeatherData(){
        
        ImageView imgView = (ImageView) currentWeatherVbox.getChildren().get(0);
        imgView.setImage(null);

        
        Label desc = (Label) currentWeatherVbox.getChildren().get(1);
        desc.setText("");

        
        Label temp = (Label) currentWeatherVbox.getChildren().get(2);
        temp.setText("");

        Label hum = (Label) currentWeatherVbox.getChildren().get(3);
        hum.setText("");
      
        Label feels = (Label) currentWeatherVbox.getChildren().get(4);
        feels.setText("");
        
        Label windSpeed = (Label) currentWeatherVbox.getChildren().get(5);
        windSpeed.setText("");

        HBox sun = (HBox) currentWeatherVbox.getChildren().get(6);

        Label sunSet = (Label) sun.getChildren().get(0);
        sunSet.setText("");
        Label sunRise = (Label) sun.getChildren().get(1);
        sunRise.setText("");
    }
    
    /**
     * set daily tiles title to default
     * @author Yassine Ibhir
     */
    private void clearDailyTileTitles(){
        for (int i = 0;i<dailyTiles.length;i++) {
            dailyTiles[i].setTitle("Day "+ (i+1));
        }
        
    }
    /**
     * set current weather tile title to default
     * @author Yassine Ibhir
     */
    private void clearCurrentWeatherTile(){
        
        this.currentWeatherTile.setTitle("Current Weather");
        
    }
    
    /**
     * this method was taken from stack over flow
     * added params to handle different dateFormat
     * @author Yassine & "https://stackoverflow.com/questions/17432735/convert-unix-timestamp-to-date-in-java"
     * @param long unix in seconds
     * @param String timeZone of city
     * @param String dateFormat
     */
    private String convertUnixTime(long unix, String timeZone,String dateFormat){    
        
        // convert seconds to milliseconds
        Date date = new java.util.Date(unix*1000L); 
        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat(dateFormat); 
        // set TimeZone
        sdf.setTimeZone(java.util.TimeZone.getTimeZone(timeZone)); 
        String formattedDate = sdf.format(date);
        // return formated date
        return formattedDate ;
        
    }
    /**
    * Stops the thread and close the application
    * @author Yassine, Grigor, and David
    **/
     public void endApplication() {
       isFinish = true;  
       Platform.exit();
       System.exit(0);
    }

        
}
