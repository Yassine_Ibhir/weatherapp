
package com.mycompany.weatherapp.dataObject;


/**
 *  an data class to help with the conversion from json
 * @author "Grigor Mihaylov"
 */
public class Temperature {
    
    private Double min;
    private Double max;

    @Override
    public String toString() {
        return "Temperature{" + "min=" + min + ", max=" + max + '}';
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }
    
    
}
