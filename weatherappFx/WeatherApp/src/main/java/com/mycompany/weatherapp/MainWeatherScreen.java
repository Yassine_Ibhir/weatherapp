package com.mycompany.weatherapp;


import eu.hansolo.tilesfx.Tile;

import java.util.Locale;
import javafx.scene.layout.*;
import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.TileBuilder;
import eu.hansolo.tilesfx.Tile.TextSize;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.image.ImageView;


/**
 * class extends Hbox, Builds all components
 * for the mainWeather Screen
 * @author Yassine, Grigor, David
 */
public class MainWeatherScreen extends HBox {
    
    private ChoiceBox choiceBox;
    private TextField cityField;
    private TextArea dht11TextArea;
    private final VBox[] daily = new VBox[7];
    private VBox currentWeather;
    private VBox alert;
    private Button exit;
    private Button update;
    private Button submit;
    private Button clear;
    private final Tile[] dailyTiles = new Tile[7];
    private  Tile currentWeatherTile;

   
    /**
     * constructor builds main screen
     */
    public MainWeatherScreen() {
        this.buildScreen();
    }
    
    /**
     * Build screen
     * @author Yassine & Grigor
     */
    private void buildScreen(){
        // Build tile to contain clock
        var clockTile = this.setClockTile();
        
        // choice
        var choiceBoxTile = this.setChoiceBoxTile();
        
        //dht11 tile
        var dht11Tile = this.setDht11Tile();
        
        //currentWeather tile
        this.currentWeatherTile = this.setCurrentWeatherTile();
        
        //alertTile
        var alertTile = this.setAlertTile();
        
        // column 1 of the dashbord
        var tilesColumn1 = new VBox(clockTile);
        tilesColumn1.setMinWidth(350);
        tilesColumn1.setSpacing(5);
        // column 2 of the dashbord
        var tilesColumn2 = new VBox(choiceBoxTile);
        tilesColumn2.setMinWidth(350);
        tilesColumn2.setSpacing(5);
        // column 3 of the dashbord
        var tilesColumn3 = new VBox(dht11Tile);
        tilesColumn3.setMinWidth(350);
        tilesColumn3.setSpacing(5);
        
         // column 4 of the dashbord
        var tilesColumn4 = new VBox(alertTile,this.currentWeatherTile);
        tilesColumn4.setMinWidth(350);
        tilesColumn4.setSpacing(5);
        
        for (int i= 0; i<daily.length; i++){
            
            var dayTile = this.setDailyTiles(i);
            // add to Tile Array
            dailyTiles[i] = dayTile;
            
            switch (i) {
                case 0:
                case 3:
                    tilesColumn1.getChildren().add(dayTile);
                    break;
                case 1:
                case 4:
                    tilesColumn2.getChildren().add(dayTile);
                    break;
                case 2:
                case 5:
                    tilesColumn3.getChildren().add(dayTile);
                    break;
                default:
                    tilesColumn4.getChildren().add(dayTile);
                    break;
            }
        }

        // add all vbox tiles to the root (Hbox)
        this.getChildren().addAll(tilesColumn1,tilesColumn2,tilesColumn3,tilesColumn4);
        this.setSpacing(2);


    }
    
    //getters for tile and Gui componenets
    
    public Tile getCurrentWeatherTile() {
        return currentWeatherTile;
    }
    public Tile[] getDailyTiles() {
        return dailyTiles;
    }
    
    public VBox getAlert() {
        return this.alert;
    }
    public ChoiceBox getChoiceBox() {
        return choiceBox;
    }
    public Button getClear() {
        return clear;
    }
    

    public TextField getCityField() {
        return cityField;
    }

    public TextArea getDht11TextArea() {
        return dht11TextArea;
    }

    public VBox[] getDaily() {
        return daily;
    }

    public VBox getCurrentWeather() {
        return currentWeather;
    }

    public Button getExit() {
        return exit;
    }

    public Button getUpdate() {
        return update;
    }

    public Button getSubmit() {
        return submit;
    }

   
    /**
     * Create the clock tile
     * @author Grigor
     */
    private Tile setClockTile(){
        // Define our local setting (used by the clock)
        var locale = new Locale("en", "CA");
        // Tile with a clock
        
        var clockTile = TileBuilder.create()
                .skinType(SkinType.CLOCK)
                .prefSize(350, 270)
                .title("Current time")
                .dateVisible(true)
                .locale(locale)
                .running(true)
                .build();
        
        return clockTile;
    }
    
    /**
     * Create the choice box tile 
     * it contains the interaction for the user
     * @author Grigor
     */
    private Tile setChoiceBoxTile(){
        //exit btn
        this.exit= new Button("Exit");
        this.exit.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
        //update
        this.update = new Button("Update");
        this.update.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
        //submit
        this.submit = new Button("Submit");
        this.submit.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
        
        //submit
        this.clear = new Button("Clear");
        this.clear.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
      
        //Create ChoiceBox
        this.choiceBox = new ChoiceBox();
        //Add the choices to the ChoiceBox
        choiceBox.getItems().add("Current Weather");
        choiceBox.getItems().add("7 Days Weather");
        choiceBox.setValue(choiceBox.getItems().get(0));
        choiceBox.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
        
        //Label for the choiceBox
        Label cbLabel = new Label("Select your choice:  ");
        cbLabel.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
        cbLabel.setTextFill(Color.WHITE);

        Label cityLabel= new Label("Type the city name: ");
        cityLabel.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");
        cityLabel.setTextFill(Color.WHITE);
        //text field for city
        this.cityField = new TextField();
        this.cityField.setStyle("-fx-font-size: 10pt;"+" -fx-font-weight: bold;");

        // all buttons
        HBox hbox3 = new HBox(this.submit,this.update,this.clear,this.exit);
        hbox3.setSpacing(10);
        hbox3.setAlignment(Pos.CENTER);
        
        // Description of the application
        Label appDesc = new Label("Weather App @Yassine, Grigor, and David"
                );
        appDesc.setWrapText(true);
        appDesc.setStyle("-fx-font-size: 10pt;");
        appDesc.setTextFill(Color.WHITE);
        
        
        //All menu components
        VBox container = new VBox(cbLabel,this.choiceBox,cityLabel,this.cityField,hbox3,appDesc);
        container.setSpacing(15);
        container.setAlignment(Pos.CENTER);
        
        //Setup the ChoiceBox tile
        var choiceboxTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 270)
                .textSize(TextSize.BIGGER)
                .title("Menu")
                .titleColor(Color.WHITE)
                .graphic(container)
                .build();
        
        return choiceboxTile;
    }

   
    /**
     * build tile for the one day of 7
     * @author Yassine
     */
    private Tile setDailyTiles(int index){
        
        //max temperature
        Label temperatureM = new Label();
        temperatureM.setTextFill(Color.YELLOW);
        temperatureM.setStyle("-fx-font-size: 16pt;"
                + "-fx-font-weight: bold;");
         
        //minimum temperature
        Label temperatureMin = new Label();
        temperatureMin.setTextFill(Color.YELLOW);
        temperatureMin.setStyle("-fx-font-size: 16pt;"
                + "-fx-font-weight: bold;");
        //humidity
        Label humidity = new Label ();
        humidity.setTextFill(Color.WHITE);
        humidity.setStyle("-fx-font-size: 16pt");

        //weathre discription
        Label description = new Label();
        description.setTextFill(Color.WHITE);
        description.setStyle("-fx-font-size: 16pt");
        
        //wind speed
        Label windSpeed = new Label();
        windSpeed .setTextFill(Color.WHITE);
        windSpeed .setStyle("-fx-font-size: 16pt");
        //time of sunrise
        Label sunRise = new Label();
        sunRise.setTextFill(Color.web("#ffa700"));
        sunRise.setStyle("-fx-font-size: 12pt;");
        sunRise.setWrapText(true);
        //time of sunset
        Label sunSet = new Label();
        sunSet.setTextFill(Color.web("#fd5e53"));
        sunSet.setStyle("-fx-font-size: 12pt;");
        sunSet.setWrapText(true);
        
        HBox sun = new HBox(sunRise,sunSet);
        sun.setAlignment(Pos.CENTER);
        sun.setSpacing(7);
        ImageView imgView = new ImageView();
         
      
        VBox day = new VBox(imgView,description,temperatureM,temperatureMin,humidity,windSpeed,sun);
        daily[index] = day;
         day.setSpacing(10);
         day.setAlignment(Pos.CENTER);
         int dayNumber = index + 1;
         //Setup the tile
         var dailyTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 380)
                .textSize(TextSize.BIGGER)
                .title("Day " + dayNumber)
                .titleColor(Color.WHITE)
                .graphic(day)
                .build(); 
         
         return dailyTile;

    }
    
    
    /**
     * create the tile to display the info from DHT11
     * @author Grigor 
     */
    private Tile setDht11Tile(){
        //area to display the info
        this.dht11TextArea= new TextArea();
        this.dht11TextArea.setStyle("-fx-control-inner-background: #2A2A2A; "
                 + "-fx-text-inner-color: #1E90FF;"
                 + "-fx-text-box-border: transparent;"
                 + "-fx-font-size: 10pt;"
                  );
        this.dht11TextArea.setEditable(false);
        //create tile
        var dht11Tile= TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 270)
                .textSize(TextSize.BIGGER)
                .title("Real Time Humidity and Temperature:")
                .graphic(this.dht11TextArea)
                .build();
        
        return dht11Tile;
    }
    /**
     * creates the tile for current weather
     * @author Yassine 
     */
    
    private Tile setCurrentWeatherTile(){
        //temperature
        Label temperature = new Label();
        temperature.setTextFill(Color.YELLOW);
        temperature.setStyle("-fx-font-size: 16pt;"
                + "-fx-font-weight: bold;");
        //humidity
        Label humidity = new Label ();
        humidity.setTextFill(Color.WHITE);
        humidity.setStyle("-fx-font-size: 16pt");
        
        Label feelsLike = new Label ();
        feelsLike.setTextFill(Color.WHITE);
        feelsLike.setStyle("-fx-font-size: 16pt");
        //what the temputure feels like
        Label description = new Label();
        description.setTextFill(Color.WHITE);
        description.setStyle("-fx-font-size: 16pt");
        //wind speed
        Label windSpeed = new Label();
        windSpeed.setTextFill(Color.WHITE);
        windSpeed.setStyle("-fx-font-size: 16pt");
        //time of sunrise
        Label sunRise = new Label();
        sunRise.setTextFill(Color.web("#ffa700"));
        sunRise.setStyle("-fx-font-size: 12pt;");
        sunRise.setWrapText(true);
        //time of sunset
        Label sunSet = new Label();
        sunSet.setTextFill(Color.web("#fd5e53"));
        sunSet.setStyle("-fx-font-size: 12pt;");
        sunSet.setWrapText(true);
        
        HBox sun = new HBox(sunRise,sunSet);
        sun.setAlignment(Pos.CENTER);
        sun.setSpacing(5);
        ImageView imgView = new ImageView();
       
        
        currentWeather = new VBox(imgView,description,temperature,humidity,feelsLike,windSpeed,sun);
        currentWeather.setSpacing(10);
        currentWeather.setAlignment(Pos.CENTER);
         //Setup the tile
         var dailyTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 380)
                .textSize(TextSize.BIGGER)
                .title("Current Weather")
                .titleColor(Color.WHITE)
                .graphic(currentWeather)
                .build(); 
         
         return dailyTile;
    
    }
    /**
     * builds alert tile to display alerts
     * @author Yassine 
     */
    private Tile setAlertTile(){
        //time zone label
        Label timeZone = new Label();
        timeZone.setTextFill(Color.WHITE);
        timeZone.setStyle("-fx-font-size: 14pt;");
        timeZone.setWrapText(true);
        //description of the alert
        TextArea description = new TextArea("Description : ");
        description.setWrapText(true);
        description.setEditable(false);
        description.setStyle("-fx-font-size: 12pt;"
                + "-fx-text-inner-color:#ffa36d;");
        
        
        this.alert = new VBox(timeZone,description);
        this.alert.setAlignment(Pos.CENTER);
        this.alert.setSpacing(15);
        //set up tile
        var alertTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 270)
                .textSize(TextSize.BIGGER)
                .title("Alert ")
                .titleColor(Color.WHITE)
                .graphic(alert)
                .build(); 

         return alertTile;
        
    }

    
}
