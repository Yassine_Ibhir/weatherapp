
package com.mycompany.weatherapp.dataObject;


/**
 *  an data class to help with the conversion from json
 * @author "Grigor Mihaylov & Yassine Ibhir"
 */
public class Current {
    private Double temp;
    private Double feels_like;
    private Double humidity;
    private long dt;
    private long sunset;
     private long sunrise;
    private Double wind_speed;
    private Weather[] weather;
    
    public long getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public long getSunset() {
        return sunset;
    }

    public void setSunset(int sunset) {
        this.sunset = sunset;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public Double getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(Double wind_speed) {
        this.wind_speed = wind_speed;
    }
   

    @Override
    public String toString() {
        return "Current{" + "temp=" + temp + ", feels_like=" + feels_like + ", humidity=" + humidity + ", dt=" + dt + ", sunset=" + sunset + ", sunrise=" + sunrise + ", wind_speed=" + wind_speed + '}';
    }

    

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getFeels_like() {
        return feels_like;
    }

    public void setFeels_like(Double feels_like) {
        this.feels_like = feels_like;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }
    
    
}
