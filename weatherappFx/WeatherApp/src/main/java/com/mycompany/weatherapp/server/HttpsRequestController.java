    package com.mycompany.weatherapp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class contains methods to normalize and validate a given URL and its parameters,
 * before making a HTTP request. It holds two private fields including a HttpURLConnection object 
 * and an object of StringBuilder to collect and return all of the JSON result data.
 * @author David Pizzolongo
 */
public class HttpsRequestController {

    private HttpURLConnection conn;
    private StringBuilder cityData;
   

    /**
    * The sendRequest method takes as input the URL of the API call, and a specific
    * query string, and creates a URL object if the query parameters are valid. 
    * If they are not valid, an IllegalArgumentException is thrown with an appropriate message.
    * It then opens the connection and if it receives a response code of 200, it invokes
    * setCityData(). This method handles any exceptions that occur and
    * returns the output of the request.
    * @param {website} string representing the OpenWeatherApp FQDN 
    * @param {params} string containing the query string to be appended to website
    * @return {cityData} string holding the formatted JSON data 
    */
    public String sendRequest(String website, String params) {
        cityData =  new StringBuilder();
        try {
            if (!isValidParams(params)) {
                throw new IllegalArgumentException("Invalid url parameters, must be key value pairs.");
            }

            URL url = new URL(website + params);
            conn = (HttpURLConnection) url.openConnection();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                setCityData();
            } else {
                System.err.println("Invalid response code.");
            }

        } catch (IllegalArgumentException illegalEx) {
            System.out.println(illegalEx);
        } catch (IOException e) {       
            System.out.println("JSON data could not be processed.");    // setCityData() error
        }
        
        return cityData.toString();     // could be an empty string (if the conn could not be made)
    }

    /**
    * Called by the sendRequest function, the isValidParams method 
    * normalizes the query string to the unique NFKC format and 
    * verifies that the normalized string matches the regex pattern. 
    * If it is valid, it returns true and if not, it returns false. 
    * @param {params} an invalidated query string
    */
    private boolean isValidParams(String params) { 
        params = Normalizer.normalize(params, Normalizer.Form.NFKC);
        
        /* valid pattern: ? _key_=_value_&  (key can only contain lowercase letters
        and value can contain letters, numbers, decimal or a comma (for city names)). 
        Each key value pair ends with an ampersand. */
        Pattern pattern = Pattern.compile("\\?([a-z]+=[A-Za-z0-9.,]+&)+");
        Matcher matcher = pattern.matcher(params);
        
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    /**
    * This function uses the HTTP connection object (socket) established above to 
    * read the output and append each line of data to the cityData, along with an empty line.
    * It throws an IOException if the input from the response is invalid or is null.  
    * Once the processing is done and cityData is complete, the BufferedReader object is closed.   
    */
    private void setCityData() throws IOException {
        InputStream inStream = conn.getInputStream();

        BufferedReader in = new BufferedReader(new InputStreamReader(inStream));
        String line;

        while ((line = in.readLine()) != null) {
            cityData.append("\n" + line);
        }
        in.close();
    }
}
