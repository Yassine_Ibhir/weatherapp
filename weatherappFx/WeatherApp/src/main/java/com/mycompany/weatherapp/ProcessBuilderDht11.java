package com.mycompany.weatherapp;

import java.io.IOException;

/**
 * creates the process that handles DHT11
 * @author David Pizzolongo
 */
public class ProcessBuilderDht11 {
    private final ProcessBuilder processBuilder;
    private String appCmd;
    
    public ProcessBuilderDht11(String appCmd){
        this.appCmd = appCmd;
        this.processBuilder = new ProcessBuilder(appCmd);
    }
    
    // for test purposes only
    public String getCmd(){
        return appCmd;
    }
    
    public Process getProcess() throws IOException{
        var process = this.processBuilder.start();
        return process;
    }
    
 
}
