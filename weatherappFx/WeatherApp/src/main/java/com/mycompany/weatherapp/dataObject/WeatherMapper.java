
package com.mycompany.weatherapp.dataObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.weatherapp.server.HttpsRequestController;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javafx.collections.FXCollections;


 /*
 * class uses object mapper to convert json
 * data into objects
 * @author Yassine Ibhir
 */
public class WeatherMapper {
    
    // instance of ObjectMapper
    private ObjectMapper objectMapper;
    
    // path cities json data 
    private static final String JSON_CITY_FILE = "src//main//resources//json_files//city.list.json";
    
    // stores cities
    private List<City> cities;
    
    // stores weather data
    private WeatherJson weatherObj;
    
    // Api Key
    private static final  String apiKey = "f7de5b9c4a694ea2687d1d0d1ddbd9de";
    
    // weather api website
    private static final String website  = "https://api.openweathermap.org/data/2.5/onecall";
    /**
     * Constructor initializes and configures
     * Object Mapper
     */
    public WeatherMapper(){
       
        objectMapper = new ObjectMapper(); 
        objectMapper.configure(
                    DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        
    }
    
    /**
     * uses Object Mapper to get cities from Json file
     * @return list of cities
     * @author Yassine Ibhir
     */
    public List<City> getCities(){
        
        try {

           File file = new File(JSON_CITY_FILE);

           cities = objectMapper.readValue(file,new TypeReference<List<City>>(){});
            
           
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
        
        return cities;
        
    }
    
    /**
     * uses Object Mapper to request weather data
     * @param coord
     * @return weatherJson Object
     * @author Yassine Ibhir
     */
    public WeatherJson getWeatherJson(Map<String, Double> coord) {
        
        // concatenate coordinates
        double lat = coord.get("lat");
        double lon = coord.get("lon");
        String coorParam = "&lat="+lat+"&lon="+lon;

        // concatenate paramaters
        String query = "?units=metric&exclude=minutely,hourly&appid="+apiKey+coorParam; 
        
        // request data
        HttpsRequestController httpController = new HttpsRequestController();
        
        String weatherData = httpController.sendRequest(website, query);
       
        try {
           
           // map to the the weatherJson object
           weatherObj = objectMapper.readValue(weatherData,WeatherJson.class);
            
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
        
        return weatherObj;
    }
   
}
