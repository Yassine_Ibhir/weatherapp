
package com.mycompany.weatherapp.dataObject;


/**
 * an data class to help with the conversion from json
 * @author "Grigor Mihaylov"
 */
public class Weather {

    
    private String description;
    private String icon;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    @Override
    public String toString() {
        return "Weather{" + "description=" + description + ", icon=" + icon + '}';
    }
}
