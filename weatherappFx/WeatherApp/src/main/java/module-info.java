module com.mycompany.weatherapp {
    requires javafx.controls;
    requires javafx.web;
    requires java.logging;
    requires eu.hansolo.tilesfx;
    exports com.mycompany.weatherapp;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires org.apache.commons.lang3;
    exports com.mycompany.weatherapp.dataObject to com.fasterxml.jackson.databind;
    requires org.controlsfx.controls;
    requires javafx.graphicsEmpty;  
}
