# WeatherApp Structure
This java project contains three main packages: weatherapp, weatherapp.dataObject and weatherapp.server. 
Weatherapp contains all the classes required to initialize and display the GUI elements on the Stage, while handling 
user actions such as submitting a city name, updating the city name, clearing the city textfield or exiting the application. 
The WeatherController class represents the interface between the view and the model, and also handles the interaction
with the ProcessBuilder class to retrieve and display the data provided by the DHT11 sensor. 

The weatherapp.dataObject contains important classes to extract the data from the JSON API file, format it and parse it into
many data object classes. This package handles most of the input/output processing of the program.
  
In addition, there are several test classes testing the back-end functionalities and possible exceptions to be thrown;
for instance, the ProcessBuilderDht11Test class and the HttpsRequestControllerTest class.

## How to run
Launch Netbeans or any other IDE that supports Maven, and open the java-with-maven project. Clean and build the 
project, and run the App.java file. 


